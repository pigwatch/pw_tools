THIS=$(readlink -f ${BASH_SOURCE[0]})
THIS_DIR=$(dirname $THIS)

export PYTHONPATH=$THIS_DIR/python:$PYTHONPATH
export PATH=$THIS_DIR/bin:$PATH
