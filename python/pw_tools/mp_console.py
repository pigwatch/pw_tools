import serial
import time

class SerialConnection:
    def __init__(self, port, baudrate=115200, debug=False):
        self._debug = debug
        self._fd = serial.Serial(port, 115200, timeout=1.5)
        self._had_line_break = False
        self._at_line_start = False
        self.collect = None

    def _read(self, n):
        a = time.time()
        ret = self._fd.read(n)
        b = time.time() - a
        self._at_line_start = self._had_line_break
        self._had_line_break = ret[-1:] == b"\n"
        if self.collect is not None:
            self.collect.append(ret)
        if self._debug: print("read after %.2fs: %r" % (b, ret))
        return ret

    def _slow_write(self, data):
        per_byte = 1. / (115200 / 10)
        bs = 64
        o = 0
        while o < len(data):
            block = data[o:o+bs]
            self._fd.write(block)
            time.sleep(1.5 * per_byte * len(block))
            o += bs

    def _write(self, what, read_echo=True):
        #print("write %r" % what)
        if self._debug:
            print("write: %r" % what)
            if isinstance(what, str):
                whats = what
            else:
                whats = what.decode("utf-8")
            print("write:\n%s--" % whats)
        if isinstance(what, str):
            what = what.encode("utf-8")
        self._slow_write(what)
        #self._fd.write(what)
        if not read_echo:
            return
        echo = self._read(len(what))
        #print("got echo: %r" % echo)

    def write(self, what, read_echo=False):
        self._write(what, read_echo=read_echo)

    def read(self, n):
        return self._read(n)

    def flush_read(self):
        self._fd.flushInput()
        return
        while True:
            data = self._read(1)
            if not data:
                return

    def wait(self, what, timeout=4, at_line_beginning=True):
        if isinstance(what, str):
            what = what.encode("utf-8")
        if timeout is None:
            deadline = None
        else:
            deadline = time.time() + timeout
        pos = 0
        while True:
            char = self._read(1)
            #print("wait: pos: %r, at_line_start %s, char: %r, expect %r" % (pos, at_line_start, char, what[pos:pos+1]))
            if deadline is not None and time.time() > deadline:
                raise Exception("timeout")
            if not char:
                continue
            if char == what[pos:pos+1] and (not at_line_beginning or pos > 0 or self._at_line_start):
                pos += 1
                if pos == len(what):
                    return
            else:
                pos = 0

def connect(con, debug=False):
    if "://" in con:
        scheme, path = con.split("://", 1)
    else:
        # try to autodetect scheme
        scheme = "serial"
        path = con
        if con.startswith("/dev/tty") or con.startswith("/dev/serial/"):
            pass
        else:
            print("warning: assuming %s is a serial port" % path)
    return eval("%sConnection(%r, debug=%r)" % (scheme[0].upper() + scheme[1:], path, debug))

prompt = ">>> ".encode("utf-8")
class MPConsole:
    def __init__(self, connection_str, debug=False):
        self._debug = debug
        self._port = connect(connection_str, debug=debug)
        self._trigger_prompt()

    def _trigger_prompt(self):
        self._port.flush_read()
        self._port.write("\x02\x02\x02")
        self._port.write("\x02\x02\x02")
        self._port.write("\x01\r\n")
        self._port.wait("raw REPL", at_line_beginning=False)
        self._port.wait("\n>", at_line_beginning=False)

    def exec(self, code, timeout=5):
        if timeout is not None:
            deadline = time.time() + timeout
        else:
            deadline = None
        code = code.lstrip()
        code = code.encode("utf-8")
        self._port.write(code + b"\x04")
        while True:
            expect_ok = self._port._read(2)
            if expect_ok is None or expect_ok == b"":
                if deadline is None or time.time() < deadline:
                    continue
                raise Exception("timeout waiting for first ok!")
            break
        if expect_ok != b"OK":
            rest = self._port.read(100)
            if rest is None:
                rest = b""
            raise Exception("expected OK, but got: %s" % (expect_ok + rest))
        self._port.collect = []
        end_marker = b"\x04"
        self._port.wait(end_marker, timeout=timeout, at_line_beginning=False)
        out = b"".join(self._port.collect)
        out = out[:-len(end_marker)]

        self._port.collect = []
        end_marker = b"\x04>"
        self._port.wait(end_marker, timeout=timeout, at_line_beginning=False)
        err = b"".join(self._port.collect)
        err = err[:-len(end_marker)]

        if err:
            raise Exception("RemoteException:\n" + err.decode("utf-8"))

        return out

    def wait_end(self):
        # print any remaining uncollected stuff
        rest = self._port._read(1024)
        if rest:
            print("MPConsole rest:\n---\n%s---\n" % rest)

    def write_file(self, fn, data):
        bs = 64
        o = 0
        lines = []
        while o < len(data):
            ld = data[o:o+bs]
            lines.append("    fp.write(%r)" % ld)
            o += bs
        self.exec("""
with open(%r, "wb") as fp:
%s
""" % (fn, "\n".join(lines)), timeout=len(lines) * 2)

    def fs_sync(self):
        self.exec("""
import uos
uos.sync()
""", timeout=5)

    def umount(self, mp):
        self.exec("""
import uos
uos.umount(%r)
""" % mp, timeout=5)
        time.sleep(2)
