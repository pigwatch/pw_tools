import os
import sys
import argparse

from pw_tools import MPConsole

class Parser:
    def __init__(self):
        self.parser = argparse.ArgumentParser(sys.argv[0])

        default_port = os.getenv("PIGWATCH", "/dev/ttyUSB0")
        self.parser.add_argument("--port", "-p", default=default_port, help="connection string towards pigwatch")
        self.parser.add_argument("--debug", action="store_true", help="enable debug output")

        self.add_argument = self.parser.add_argument
        self.parse_args = self.parser.parse_args

    def get_con_and_args(self):
        args = self.parse_args()
        con = MPConsole(args.port, debug=args.debug)
        return con, args
