#!/usr/bin/env python3

import traceback
import sys
import os
import fnmatch

from pw_tools.argparse import Parser
from pw_tools import MPConsole

def search_remote_files(con, dst, fns):
    code = """
import os
import uos
import uhashlib
def hexdigest(p):
    h = uhashlib.sha256()
    with open(p, "rb") as fp:
        while True:
            data = fp.read(1024)
            if not data:
                break
            h.update(data)
    return h.digest()

ret = []
base = %r
fns = %r
for pp in fns:
    p = base + pp
    if p[-1:] == "/":
        p = p[:-1]
    try:
        sbuf = uos.stat(p)
    except:
        continue
    mode = sbuf[0]
    if mode & 0x8000:
        st_size = sbuf[6]
        ret.append((p[len(base):], st_size, hexdigest(p)))
        continue
    if mode & 0x4000:
        ret.append(((p + "/")[len(base):], ))
print(ret)
""" % (dst, fns)
    ret = con.exec(code, timeout=10)
    remote_files = eval(ret)
    return remote_files

def get_remote_files(con, dst):
    code = """
import os
import uos
import uhashlib
def hexdigest(p):
    h = uhashlib.sha256()
    with open(p, "rb") as fp:
        while True:
            data = fp.read(1024)
            if not data:
                break
            h.update(data)
    return h.digest()

ret = []
base = %r
def ls(dn):
    for e in os.listdir(dn):
        if dn.endswith("/"):
            p = dn + e
        else:
            p = dn + "/" + e
        sbuf = uos.stat(p)
        mode = sbuf[0]
        if mode & 0x8000:
            st_size = sbuf[6]
            ret.append((p[len(base):], st_size, hexdigest(p)))
            continue
        if e[:1] != "." and mode & 0x4000:
            ret.append(((p + "/")[len(base):], ))
            ls(p)
ls(base)
print(ret)
""" % (dst)
    try:
        ret = con.exec(code, timeout=10)
        remote_files = eval(ret)
        exists = True
    except Exception as e:
        if "ENOENT" not in str(e):
            raise
        # assume remote does not exist
        remote_files = []
        exists = False
    remote_files.sort()
    return remote_files, exists

def make_remote_dirs(con, dirs):
    #print("mkdirs", dirs)
    code = """
import os
for dn in %r:
    os.mkdir(dn)
""" % (dirs, )
    con.exec(code)

import hashlib

def get_local_files(src):
    ret = []
    def hexdigest(p):
        h = hashlib.sha256()
        with open(p, "rb") as fp:
            data = fp.read()
        h.update(data)
        return h.digest()
    def ls(dn):
        for e in os.listdir(dn):
            p = os.path.join(dn, e)
            if os.path.isfile(p):
                ret.append((p[len(src):], os.path.getsize(p), hexdigest(p)))
                continue
            if e[:1] != "." and os.path.isdir(p):
                ret.append(((p + "/")[len(src):], ))
                ls(p)
    ls(src)
    ret.sort()
    return ret

def main():
    parser = Parser()
    parser.add_argument("src", help="local source folder")
    parser.add_argument("dst", nargs="?", help="remote destination folder. basename and remote curdir will be used if not specified")
    parser.add_argument("--exclude", action="append", help="file globs to exclude")
    parser.add_argument("-v", dest="verbose", action="store_true", help="verbose debug output")
    parser.add_argument("--reboot-print", action="store_true", help="reboot after sync, then reboot and print output")
    con, args = parser.get_con_and_args()

    if args.dst is None:
        args.dst = "./" + os.path.basename(os.path.realpath(args.src))

    if not args.exclude:
        args.exclude = []
    args.exclude.append("*.pyc")
    args.exclude.append("__pycache__")

    if args.dst[-1:] != "/":
        args.dst = args.dst + "/"

    if args.verbose:
        print("remote dir: %r" % args.dst)
        print("exclude: %r" % args.exclude)

    #remote, remote_exists = get_remote_files(con, args.dst)
    local = get_local_files(args.src)


    remote = search_remote_files(con, args.dst, [e[0] for e in local])
    if remote:
        remote_exists = True
    else:
        remote_exists = False

    if args.debug:
        for e in remote:
            print("remote: %r" % (e, ))
        for e in local:
            print("local: %r" % (e, ))

    # check to create directories
    dirs_to_create = []
    if not remote_exists:
        dirs_to_create.append(args.dst[:-1])
    for e in local:
        if len(e) != 1:
            continue
        if e in remote:
            continue
        bn = os.path.basename(e[0][:-1])
        for pat in args.exclude:
            if fnmatch.fnmatch(bn, pat):
                break
        else:
            dirs_to_create.append(os.path.join(args.dst, e[0][:-1]))
            if args.verbose: print("create dir: %r" % e[0])
    # check for files to send
    files_to_send = []
    for e in local:
        if len(e) == 1:
            continue
        if e in remote:
            continue
        for pat in args.exclude:
            if fnmatch.fnmatch(os.path.basename(e[0]), pat):
                break
        else:
            files_to_send.append(e[0])
            if args.verbose: print("send file: %r" % e[0])


    make_remote_dirs(con, dirs_to_create)
    for e in files_to_send:
        sn = os.path.join(args.src, e)
        with open(sn, "rb") as fp:
            data = fp.read()
        tn = os.path.join(args.dst, e)
        if args.verbose: print("sending %r to %r" % (sn, tn))
        con.write_file(tn, data)

    #con.fs_sync()

    if args.reboot_print:
        con.umount("/flash")
        print("rebooting.")
        con._port.write(b"\x02")
        con._port.write(b"\x04")
        con._port._debug = False
        while True:
            data = con._port.read(1)
            if not data:
                continue
            sys.stdout.buffer.write(data)
            sys.stdout.flush()


if __name__ == "__main__":
    main()
